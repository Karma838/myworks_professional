/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если температура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

const BeachParty = () => {



        class Human {
            constructor(name){
                this.name = name;
                this.currentTemperature = 0;
                this.minTemperature = -10;
                this.maxTemperature = 50;

                console.log(`new Human ${this.name} arrived!`, this);
            }

            changeTemperature(changeValue){
              /* console.log(
                    'current', this.currentTemperature + changeValue,
                    'min', this.minTemperature,
                    'max', this.maxTemperature
                );*/
                let prevTemperature = this.currentTemperature;
                this.currentTemperature = this.currentTemperature + changeValue;

                if(this.currentTemperature < 0) {
                    if (this.currentTemperature < this.minTemperature) {
                        console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
                    } else {
                        if (this.currentTemperature > prevTemperature) {
                            console.log(`Temperature is growing. Seems someone go to Odessa or drink some hot tea?`)
                        } else {
                            console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
                        }
                    }
                }else if(this.currentTemperature > 0){
                 if(this.currentTemperature > this.maxTemperature ){
                        console.error(`Temperature is too high: ${this.currentTemperature}. ${this.name} fired under sun :(`);
                }else {
                     if (this.currentTemperature > prevTemperature) {
                         console.log(`Temperature is growing (${this.currentTemperature} deg ) . Seems someone go to the beach?`)
                     } else {
                         console.log(`Nice windy weather. (${this.currentTemperature} deg), Be careful ,${this.name} `);
                     }
                 }

                }
            }
        }

      /*  let Debra = new Human('Debra');
        Debra.changeTemperature(-5);
        Debra.changeTemperature(6);
        Debra.changeTemperature(-16);
*/
        class DressedHuman extends Human{
            constructor(name){
                super(name);
                this.clothes = [
                    { name: 'jacket', temperatureResistance: 20},
                    { name: 'hat', temperatureResistance: 5},
                    { name: 'scarf', temperatureResistance: 10},
                ];
                this.minTemperature = this.minTemperature - this.clothes.reduce(
                    (currentResistance, clothe ) => {
                        console.log('currentResistance', currentResistance,  'clothe', clothe);
                        return currentResistance + clothe.temperatureResistance;
                    }, 0
                );
                console.log(`Dressed Human ${name}`, this);
            }
        }

    class BeachHuman extends Human{
        constructor(name){
            super(name);
            this.coolers = [
                { name: 'icecream', temperatureCoolRate: -5},
                { name: 'soda', temperatureCoolRate: -10},
                { name: 'water', temperatureCoolRate: -8},
                { name: 'cocktail', temperatureCoolRate: -15},
            ];

            console.log(`Beach Human ${name}`, this)
        }
        changeTemperature(value) {
            let prevTemp = this.currentTemperature;
            this.currentTemperature = Number(prevTemp + value);
            if (this.currentTemperature > 30 && this.currentTemperature < this.maxTemperature) {
                let div = this.currentTemperature - this.maxTemperature;
                if (div < 5) {
                    this.currentTemperature += this.coolers[0].temperatureCoolRate;
                    console.log(`Nice windy weather. (${this.currentTemperature} deg),${this.name} ate ${ this.coolers[0].name} `);
                } else if (div > 5 && div < 8) {
                    this.currentTemperature += this.coolers[2].temperatureCoolRate;
                    console.log(`Nice windy weather. (${this.currentTemperature} deg),${this.name} drank ${ this.coolers[2].name} `);
                } else if (div > 8 && div < 10) {
                    this.currentTemperature += this.coolers[1].temperatureCoolRate;
                    console.log(`Nice windy weather. (${this.currentTemperature} deg),${this.name} drank ${ this.coolers[1].name} `);
                } else if (div > 10) {
                    this.currentTemperature += this.coolers[3].temperatureCoolRate;
                    console.log(`Nice windy weather. (${this.currentTemperature} deg),${this.name} drank ${ this.coolers[3].name} `);
                }


            } else if (this.currentTemperature > this.maxTemperature) {
                console.error(`Temperature is too high: ${this.currentTemperature}. ${this.name} fired under sun :(`);
            } else {
                console.log(`Nice  weather. (${this.currentTemperature} deg), Be careful ,${this.name} `);
            }
        }}




      /* let Masuka = new DressedHuman('Masuka');
        Masuka.changeTemperature(-25);
        Masuka.changeTemperature(-26);
*/

    let Saske = new BeachHuman('Saske');
    //Saske.changeTemperature(10);
    Saske.changeTemperature(31);
    Saske.changeTemperature(60);
   // Saske.changeTemperature(40);



}

export default BeachParty;
