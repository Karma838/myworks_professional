
export class TargetNotifier{
    constructor(name){
        this.name = name;
        this.image = `images/${this.name}.svg`;
    }
}
export default TargetNotifier;