/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const Single = ()=>{

    const _data = {
        laws: [],
        budget: 1000000,
        citizensSatisfactions: 0
    };

    const Govern = {
        addLaw: (id, name, description)=>{ _data.laws.push({id,name,description});_data.citizensSatisfactions -= 10;},
        getLaw: id => _data.laws.find( d => d.id === id ),
        showAllLaws: () => console.log(_data.laws),
        showSatisfaction: () => console.log(_data.citizensSatisfactions),
        showBudget: () => console.log(_data.budget),
        celebrate:() =>{_data.budget -= 50000;_data.citizensSatisfactions+=5;}
    };

    Govern.addLaw(1,"loca","Nice law");
    Govern.addLaw(2,"Legalize Weed","Super law");
    Govern.showAllLaws();
    Govern.showSatisfaction();
    Govern.celebrate();
    Govern.showSatisfaction();
    Govern.showBudget();
    //Object.freeze(Rule);

}
export default Single;