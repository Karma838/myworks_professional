/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/objectfreeze */ \"./classworks/objectfreeze.js\");\n/* harmony import */ var _classworks_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../classworks/singleton */ \"./classworks/singleton.js\");\n/*\n\n  Модули в JS\n  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import\n\n  Так как для экспорта и импорта нету родной поддержки в браузерах, то\n  нам понадобится сборщик или транспалер который это умеет делать.\n  -> babel, webpack, rollup\n\n  На сегодняшний день - самое полулярное решение, это вебпак!\n\n  npm i webpack webpack-cli\n\n  Установка и config-less настройка\n\n  \"scripts\": {\n    \"cli\": \"webpack ./application/index.js --output-path ./public/js --output-filename bundle.js --mode development --color --watch\"\n  }\n\n*/\n  `webpack\n      ./application/index.js\n      --output-path ./public/js\n      --output-filename bundle.js\n      --mode development\n      --color\n      --watch\n  `;\n\n/*\n\n  npm run cli\n  Затестим - в консоли наберем команду webpack\n\n*/\n\n  // import imports from './imports';\n\n  console.log('WEBPACK WORKING!')\n\n\n\n\n//Frezee();\nlet single = Object(_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/objectfreeze.js":
/*!************************************!*\
  !*** ./classworks/objectfreeze.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: написать функцию, для глубокой заморозки обьектов.\n\n  Обьект для работы:\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\n\n  Методы для работы:\n  1. Object.getOwnPropertyNames(obj);\n      -> Получаем имена свойств из объекта obj в виде массива\n\n  2. Проверка через typeof на обьект или !== null\n  if (typeof prop == 'object' && prop !== null){...}\n\n  Тестирование:\n\n  let FarGalaxy = DeepFreeze(universe);\n      FarGalaxy.good.push('javascript'); // true\n      FarGalaxy.something = 'Wow!'; // false\n      FarGalaxy.evil.humans = [];   // false\n\n*/\n\nconst work = () => {\n\n    let universe = {\n        infinity: Infinity,\n        good: ['cats', 'love', 'rock-n-roll'],\n        evil: {\n            bonuses: ['cookies', 'great look']\n        }\n    };\n\n    const DeepFreeze =( Obj )=>{\n     let massive = Object.getOwnPropertyNames(Obj);\n     massive.forEach((prop)=>{\n\n         if(typeof prop == 'object' ){\n           Object.freeze(prop);\n           console.log(prop , ' is frozen');\n         }\n\n     })\n        Object.freeze(Obj);\n        return Obj;\n    }\n\n\n    let FarGalaxy = DeepFreeze(universe);\n    try{\n        FarGalaxy.good.push('javascript'); // true\n        FarGalaxy.something = 'Wow!'; // false\n        FarGalaxy.evil.humans = [];   // false\n        console.log(FarGalaxy);\n    }catch(e){\n      console.error(e);\n    }\n\n\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (work);\n\n\n//# sourceURL=webpack:///./classworks/objectfreeze.js?");

/***/ }),

/***/ "./classworks/singleton.js":
/*!*********************************!*\
  !*** ./classworks/singleton.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:\n\n    Написать синглтон, который будет создавать обьект government\n\n    Данные:\n    {\n        laws: [],\n        budget: 1000000\n        citizensSatisfactions: 0,\n    }\n\n    У этого обьекта будут методы:\n      .добавитьЗакон({id, name, description})\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\n\n      .читатькКонституцию -> Вывести все законы на экран\n      .читатьЗакон(ид)\n\n      .показатьУровеньДовольства()\n      .показатьБюджет()\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\n\n\n*/\n\nconst Single = ()=>{\n\n    const _data = {\n        laws: [],\n        budget: 1000000,\n        citizensSatisfactions: 0\n    };\n\n    const Govern = {\n        addLaw: (id, name, description)=>{ _data.laws.push({id,name,description});_data.citizensSatisfactions -= 10;},\n        getLaw: id => _data.laws.find( d => d.id === id ),\n        showAllLaws: () => console.log(_data.laws),\n        showSatisfaction: () => console.log(_data.citizensSatisfactions),\n        showBudget: () => console.log(_data.budget),\n        celebrate:() =>{_data.budget -= 50000;_data.citizensSatisfactions+=5;}\n    };\n\n    Govern.addLaw(1,\"loca\",\"Nice law\");\n    Govern.addLaw(2,\"Legalize Weed\",\"Super law\");\n    Govern.showAllLaws();\n    Govern.showSatisfaction();\n    Govern.celebrate();\n    Govern.showSatisfaction();\n    Govern.showBudget();\n    //Object.freeze(Rule);\n\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (Single);\n\n//# sourceURL=webpack:///./classworks/singleton.js?");

/***/ })

/******/ });