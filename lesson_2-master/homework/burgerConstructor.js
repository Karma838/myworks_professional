

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS
          Сделать графическую оболочку для программы.

  */
const Burgers=()=>{
  const Ingredients = [
        'Булка',
        'Огурчик',
        'Котлетка',
        'Бекон',
        'Рыбная котлета',
        'Соус карри',
        'Кисло-сладкий соус',
        'Помидорка',
        'Маслины',
        'Острый перец',
        'Капуста',
        'Кунжут',
        'Сыр Чеддер',
        'Сыр Виолла',
        'Сыр Гауда',
        'Майонез'
    ];


  var OurMenu = [];
  var OurOrders = [];
  var OrderId = 0;
  var OrderType;

  function Burger( name, ingredients, cookingTime)  {
    this.cookingTime = cookingTime,
        this.name = name,
        this.ingredients = ingredients,
          this.showComposition = function(){
          let compositionLength = ingredients.length;
         // console.log(compositionLength);
          if( compositionLength !== 0){
              ingredients.map( function( item ){
                  console.log(  item );
              })
          }
    }
      OurMenu.splice(0,0,this);

  }

  function Order(id, name, condition, value){
      this.orderNumber = id,
          this.orderBurder =  name,
          this.condition =  condition,
          this.makeOrder = function () {

        if(condition == null && value == null){
            let myburger;
                OurMenu.forEach(item => {
           if(item.name === this.orderBurder){
             myburger = item;
           };
          });

          console.log("Order ",this.orderNumber,". Ваш ",this.orderBurder," будет готов через ",myburger.cookingTime," минут");

        }
        else if(this.condition === "has" && this.orderBurder == "")
        {
            let myburger;
            let flag = false;
            OurMenu.forEach(burg => {
                burg.ingredients.forEach((item)=>{

                  if(item === value){
                    myburger = burg;
                    flag = true;
                  }
                })


            });
            if(flag){
                console.log("Order ",this.orderNumber,". Ваш ",myburger.name," c ",value," будет готов через ",myburger.cookingTime," минут");
            }else{
                console.log("Такого у нас в меню нету. Предлагаю выбрать другой бургер ,например -  Hamburger");
            }

        }
        else if(this.condition === "except"  && this.orderBurder == "")
        {

            let myburger;
            let flag = false;
            OurMenu.forEach(burg => {
                burg.ingredients.forEach((item)=>{
                  if(value === "Булка"){
                    console.log("Так нельзя.");
                  }
                  else
                    {
                      if(item === value){

                          this.orderBurder = myburger.name;
                          myburger = burg;
                         delete myburger.ingredients[item.indexOf()];
                          flag = true;
                      }
                  }


                })
            });
            if(flag){
                console.log("Order ",this.orderNumber,". Ваш ",myburger.name," без ",value," будет готов через ",myburger.cookingTime," минут");
            }else{
                console.log("Такого у нас в меню нету.");
            }

        }

          }
      OurOrders.splice(0,0,this);
      OurOrders.reverse();
      console.log(OurOrders);
  }

    let hamburger = new Burger("Hamburger",[Ingredients[0],Ingredients[1],Ingredients[2],'Сыр Чеддер','Майонез',Ingredients[0]],20);
    let cheeseburger = new Burger("Cheeseburger",[Ingredients[0],'Огурчик','Котлетка','Сыр Гауда','Бекон','Майонез',"Капуста",Ingredients[0]],30);
    let fishburger = new Burger("Fishburger",[Ingredients[0],'Рыбная котлета','Маслины','Сыр Виолла','Майонез',Ingredients[0]],35);
    let doublecheeseburger = new Burger("Double Cheeseburger",[Ingredients[0],'Огурчик','Сыр Гауда','Котлетка','Сыр Чеддер','Майонез',Ingredients[0]],30);
    let luxuryhamburger = new Burger("Luxury Hamburger",[Ingredients[0],'Огурчик','Помидорка','Котлетка',
        'Бекон','Сыр Чеддер','Соус карри',Ingredients[0]],20);




window.onload=()=>{
    let submit = document.getElementById("order");
    let radioes = document.getElementsByName("radio");
    let burgerName = document.getElementById('burgerInput');
    console.log(radioes);
    let condition = document.getElementById('condition');
    radioes.forEach((item)=>{
      item.addEventListener("click",()=>{

          if(item.dataset.condition === "no"){
              burgerName.removeAttribute("hidden");
              condition.setAttribute("hidden","hidden");

               OrderType = 1;

          }else if(item.dataset.condition === "has"){
              burgerName.setAttribute("hidden","hidden");
              condition.removeAttribute("hidden");

              OrderType = 2;
          }
          else if(item.dataset.condition === "except"){
              burgerName.setAttribute("hidden","hidden");
              condition.removeAttribute("hidden");

              OrderType = 3;
          }

      })
    })


    submit.addEventListener("click",()=>{

        let conditionValue = condition.value;
        if(OrderType === 1){

            let order = new Order(OurOrders.length + 1,burgerName.value);
            order.makeOrder();
        }else if(OrderType === 2){
            burgerName.setAttribute("hidden","hidden");
          burgerName.value = "";
            let order = new Order(OurOrders.length + 1,"","has",conditionValue);
            order.makeOrder();
        }else if(OrderType === 3){
            burgerName.setAttribute("hidden","hidden");
            burgerName.value = "";
            let order = new Order(OurOrders.length + 1,"","except",conditionValue);
            order.makeOrder();
        }
    })

  }









/* OurMenu.map((item)=>{
   console.log("Состав",item.name);
   item.showComposition();
 });

*/
    console.log(OurMenu);
 //let order1 = new Order(OurOrders.length + 1,"Hamburger");

/* let order2 = new Order(OurOrders.length + 1,"","except","Котлетка");

 let order3 = new Order(OurOrders.length + 1,"","except","Булка");
    order2.makeOrder();
    order3.makeOrder();
*/






};
export default Burgers();