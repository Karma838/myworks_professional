/*

  Задание:

  Написать функцию конструктор, которая будет иметь приватные и публичные свойства.
  Публичные методы должны вызывать приватные.

  Рассмотрим на примере планеты:

    - На вход принимаются параметр Имя планеты.

    Создается новый обьект, который имеет публичные методы и свойства:
    - name (передается через конструктор)
    - population ( randomPopulation());
    - rotatePlanet(){
      let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
      if ( (randomNumber % 2) == 0) {
        growPopulation();
      } else {
        Cataclysm();
      }
    }

    Приватное свойство:
    populationMultiplyRate - случайное число от 1 до 10;

    Приватные методы
    randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000
    growPopulation() {
      функция которая берет приватное свойство populationMultiplyRate
      которое равняется случайному числу от 1 до 10 и умножает его на 100.
      Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
      что за один цикл прибавилось столько населения на планете .
    }
    Cataclysm(){
      Рандомим число от 1 до 10 и умножаем его на 10000;
      То число которое получили, отнимаем от популяции.
      В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
    }

*/
const planets =()=>
{
    function planetGenerator(name) {
        const randomPopulation = () => GenerateRandomNum(1000000);
        var population = randomPopulation();
        this.populationMultiply = GenerateRandomNum(10);
        this.name = name;
        this.showPop = function () {
            console.log('Population:', population);
        };


        function GenerateRandomNum(q) {
            return Math.floor(Math.random() * q);
        }


        const growPopulation = () => {

            population += this.populationMultiply * 100;
            console.log('Population after 1 period: ', population);
        }
        const Cataclysm = () => {
            let death = this.populationMultiply * 10000;
            population -= death;
            if (population < 0) {
                population = 0;
                console.log('Humanity is dead: ', population);
            } else {
                console.log('After Cataclysm is alive : ', population);
            }
        }


        this.rotatePlanet = () => {
            let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
            if ((randomNumber % 2) == 0) {
                growPopulation();
            } else {
                Cataclysm();
            }
        }
    }

    var Planet = prompt("Введите название вашей планеты: ", name);
    var btn = document.getElementById("Rotate");
    var myPlanet = new planetGenerator(Planet);
    myPlanet.showPop();
    btn.addEventListener("click", () => {
        myPlanet.rotatePlanet();


    })
}
export default planets();