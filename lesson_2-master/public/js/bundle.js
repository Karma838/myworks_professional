/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _homework_burgerConstructor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../homework/burgerConstructor */ \"./homework/burgerConstructor.js\");\n/*\n\n  Модули в JS\n  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import\n\n  Так как для экспорта и импорта нету родной поддержки в браузерах, то\n  нам понадобится сборщик или транспалер который это умеет делать.\n  -> babel, webpack, rollup\n\n  На сегодняшний день - самое полулярное решение, это вебпак!\n\n  npm i webpack webpack-cli\n\n  Установка и config-less настройка\n\n  \"scripts\": {\n    \"cli\": \"webpack ./application/index.js --output-path ./public/js --output-filename bundle.js --mode development --color --watch\"\n  }\n\n*/\n  `webpack\n      ./application/index.js\n      --output-path ./public/js\n      --output-filename bundle.js\n      --mode development\n      --color\n      --watch\n  `;\n\n/*\n\n  npm run cli\n  Затестим - в консоли наберем команду webpack\n\n*/\n\n\n\n//import planets from \"../homework/planetsConstructor\";\n\n//planets();\nObject(_homework_burgerConstructor__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./homework/burgerConstructor.js":
/*!***************************************!*\
  !*** ./homework/burgerConstructor.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\n\n  /*\n\n    Задание:\n\n      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню\n\n      Дожно выглядеть так:\n      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);\n\n      Моделька для бургера:\n      {\n        cookingTime: 0,     // Время на готовку\n        showComposition: function(){\n          let {composition, name} = this;\n          let compositionLength = composition.length;\n          console.log(compositionLength);\n          if( compositionLength !== 0){\n            composition.map( function( item ){\n                console.log( 'Состав бургера', name, item );\n            })\n          }\n        }\n      }\n\n      Результатом конструктора нужно вывести массив меню c добавленными элементами.\n      // menu: [ {name: \"\", composition: [], cookingTime:\"\"},  {name: \"\", composition: [], cookingTime:\"\"}]\n\n        2. Создать конструктор заказов\n\n        Моделька:\n        {\n          id: \"\",\n          orderNumber: \"\",\n          orderBurder: \"\",\n          orderException: \"\",\n          orderAvailability: \"\"\n        }\n\n          Заказ может быть 3х типов:\n            1. В котором указано название бургера\n              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.\n            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант\n              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.\n            3. В котором указано чего не должно быть\n              new Order('', 'except', 'Название ингредиента') ->\n\n\n            Каждый их которых должен вернуть статус:\n            \"Заказ 1: Бургер ${Название}, будет готов через ${Время}\n\n            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:\n              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать \"Чизбургер\"\n              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ\n              Если нет, предложить еще вариант из меню.\n\n        3. Протестировать программу.\n          1. Вначале добавляем наши бургеры в меню (3-4 шт);\n          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню\n          3. Формируем 3 заказа\n\n        Бонусные задания:\n        4. Добавлять в исключения\\пожелания можно несколько ингредиентов\n        5. MEGABONUS\n          Сделать графическую оболочку для программы.\n\n  */\nconst Burgers=()=>{\n  const Ingredients = [\n        'Булка',\n        'Огурчик',\n        'Котлетка',\n        'Бекон',\n        'Рыбная котлета',\n        'Соус карри',\n        'Кисло-сладкий соус',\n        'Помидорка',\n        'Маслины',\n        'Острый перец',\n        'Капуста',\n        'Кунжут',\n        'Сыр Чеддер',\n        'Сыр Виолла',\n        'Сыр Гауда',\n        'Майонез'\n    ];\n\n\n  var OurMenu = [];\n  var OurOrders = [];\n  var OrderId = 0;\n  var OrderType;\n\n  function Burger( name, ingredients, cookingTime)  {\n    this.cookingTime = cookingTime,\n        this.name = name,\n        this.ingredients = ingredients,\n          this.showComposition = function(){\n          let compositionLength = ingredients.length;\n         // console.log(compositionLength);\n          if( compositionLength !== 0){\n              ingredients.map( function( item ){\n                  console.log(  item );\n              })\n          }\n    }\n      OurMenu.splice(0,0,this);\n\n  }\n\n  function Order(id, name, condition, value){\n      this.orderNumber = id,\n          this.orderBurder =  name,\n          this.condition =  condition,\n          this.makeOrder = function () {\n\n        if(condition == null && value == null){\n            let myburger;\n                OurMenu.forEach(item => {\n           if(item.name === this.orderBurder){\n             myburger = item;\n           };\n          });\n\n          console.log(\"Order \",this.orderNumber,\". Ваш \",this.orderBurder,\" будет готов через \",myburger.cookingTime,\" минут\");\n\n        }\n        else if(this.condition === \"has\" && this.orderBurder == \"\")\n        {\n            let myburger;\n            let flag = false;\n            OurMenu.forEach(burg => {\n                burg.ingredients.forEach((item)=>{\n\n                  if(item === value){\n                    myburger = burg;\n                    flag = true;\n                  }\n                })\n\n\n            });\n            if(flag){\n                console.log(\"Order \",this.orderNumber,\". Ваш \",myburger.name,\" c \",value,\" будет готов через \",myburger.cookingTime,\" минут\");\n            }else{\n                console.log(\"Такого у нас в меню нету. Предлагаю выбрать другой бургер ,например -  Hamburger\");\n            }\n\n        }\n        else if(this.condition === \"except\"  && this.orderBurder == \"\")\n        {\n\n            let myburger;\n            let flag = false;\n            OurMenu.forEach(burg => {\n                burg.ingredients.forEach((item)=>{\n                  if(value === \"Булка\"){\n                    console.log(\"Так нельзя.\");\n                  }\n                  else\n                    {\n                      if(item === value){\n\n                          this.orderBurder = myburger.name;\n                          myburger = burg;\n                         delete myburger.ingredients[item.indexOf()];\n                          flag = true;\n                      }\n                  }\n\n\n                })\n            });\n            if(flag){\n                console.log(\"Order \",this.orderNumber,\". Ваш \",myburger.name,\" без \",value,\" будет готов через \",myburger.cookingTime,\" минут\");\n            }else{\n                console.log(\"Такого у нас в меню нету.\");\n            }\n\n        }\n\n          }\n      OurOrders.splice(0,0,this);\n      OurOrders.reverse();\n      console.log(OurOrders);\n  }\n\n    let hamburger = new Burger(\"Hamburger\",[Ingredients[0],Ingredients[1],Ingredients[2],'Сыр Чеддер','Майонез',Ingredients[0]],20);\n    let cheeseburger = new Burger(\"Cheeseburger\",[Ingredients[0],'Огурчик','Котлетка','Сыр Гауда','Бекон','Майонез',\"Капуста\",Ingredients[0]],30);\n    let fishburger = new Burger(\"Fishburger\",[Ingredients[0],'Рыбная котлета','Маслины','Сыр Виолла','Майонез',Ingredients[0]],35);\n    let doublecheeseburger = new Burger(\"Double Cheeseburger\",[Ingredients[0],'Огурчик','Сыр Гауда','Котлетка','Сыр Чеддер','Майонез',Ingredients[0]],30);\n    let luxuryhamburger = new Burger(\"Luxury Hamburger\",[Ingredients[0],'Огурчик','Помидорка','Котлетка',\n        'Бекон','Сыр Чеддер','Соус карри',Ingredients[0]],20);\n\n\n\n\nwindow.onload=()=>{\n    let submit = document.getElementById(\"order\");\n    let radioes = document.getElementsByName(\"radio\");\n    let burgerName = document.getElementById('burgerInput');\n    console.log(radioes);\n    let condition = document.getElementById('condition');\n    radioes.forEach((item)=>{\n      item.addEventListener(\"click\",()=>{\n\n          if(item.dataset.condition === \"no\"){\n              burgerName.removeAttribute(\"hidden\");\n              condition.setAttribute(\"hidden\",\"hidden\");\n\n               OrderType = 1;\n\n          }else if(item.dataset.condition === \"has\"){\n              burgerName.setAttribute(\"hidden\",\"hidden\");\n              condition.removeAttribute(\"hidden\");\n\n              OrderType = 2;\n          }\n          else if(item.dataset.condition === \"except\"){\n              burgerName.setAttribute(\"hidden\",\"hidden\");\n              condition.removeAttribute(\"hidden\");\n\n              OrderType = 3;\n          }\n\n      })\n    })\n\n\n    submit.addEventListener(\"click\",()=>{\n\n        let conditionValue = condition.value;\n        if(OrderType === 1){\n\n            let order = new Order(OurOrders.length + 1,burgerName.value);\n            order.makeOrder();\n        }else if(OrderType === 2){\n            burgerName.setAttribute(\"hidden\",\"hidden\");\n          burgerName.value = \"\";\n            let order = new Order(OurOrders.length + 1,\"\",\"has\",conditionValue);\n            order.makeOrder();\n        }else if(OrderType === 3){\n            burgerName.setAttribute(\"hidden\",\"hidden\");\n            burgerName.value = \"\";\n            let order = new Order(OurOrders.length + 1,\"\",\"except\",conditionValue);\n            order.makeOrder();\n        }\n    })\n\n  }\n\n\n\n\n\n\n\n\n\n/* OurMenu.map((item)=>{\n   console.log(\"Состав\",item.name);\n   item.showComposition();\n });\n\n*/\n    console.log(OurMenu);\n //let order1 = new Order(OurOrders.length + 1,\"Hamburger\");\n\n/* let order2 = new Order(OurOrders.length + 1,\"\",\"except\",\"Котлетка\");\n\n let order3 = new Order(OurOrders.length + 1,\"\",\"except\",\"Булка\");\n    order2.makeOrder();\n    order3.makeOrder();\n*/\n\n\n\n\n\n\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (Burgers());\n\n//# sourceURL=webpack:///./homework/burgerConstructor.js?");

/***/ })

/******/ });