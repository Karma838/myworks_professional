/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/hoc.js":
/*!****************************!*\
  !*** ./application/hoc.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\n/*\n\n  Higher Order Functions\n  function is a values\n\n*/\nlet hofDemo = () => {\n\n  let multiply = x => x * x;\n  let nine = multiply(3);\n  console.log( 'multiply:', nine );\n\n  /*\n\n    Array.filter (так же как map, forEach, etc...) пример использования HOF в нативном js\n    Паттерн позворялет использовать композцицию что бы собрать из маленьких функций одну большую\n\n  */\n\n  let zoo = [\n    {id:0, name:\"WoofMaker\", species: 'dog'},\n    {id:1, name:\"WhiteFurr\", species: 'rabbit'},\n    {id:2, name:\"MeowMaker\", species: 'cat'},\n    {id:3, name:\"PoopMaker\", species: 'dog'},\n    {id:4, name:\"ScratchMaker\", species: 'cat'},\n  ]\n\n  let isDog = animal => animal.species === 'dog';\n  let isCat = animal => animal.species === 'cat';\n\n  let dogs = zoo.filter( isDog );\n  let cats = zoo.filter( isCat );\n\n  console.log('Here dogs:', dogs);\n  console.log('Here cats:', cats);\n\n  // - - - - - - - - - - - - - - - - - -\n\n  function compose(func_a, func_b){\n    return function(c){\n      return func_a( func_b(c) );\n    }\n  }\n  const addTwo = value => {\n    console.log('Add', value);\n    return value + 2\n  }\n  const multiplyTwo = value => {\n    console.log('Mulitple', value);\n    return value * 2;\n  }\n\n  const addTwoAndMultiplayTwo = compose( addTwo, multiplyTwo );\n  // addTwoAndMultiplayTwo( 10 )\n\n  /*\n    В данном случае происходит следующее:\n    - Вызывается ф-я compose которая принимает ф-и addTwo, multiplyTwo как аргументы\n    - Вызывается функция которая передана как аргумент func_b\n    - Результат её выполнения передается в функция func_a\n    - Общий результат возвращается в ф-ю которая нам возвращается в переменную\n  */\n\n  // console.log(\n  //   addTwoAndMultiplayTwo(2),\n  //   addTwoAndMultiplayTwo(6),\n  //   addTwoAndMultiplayTwo(40)\n  // );\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (hofDemo);\n\n\n//# sourceURL=webpack:///./application/hoc.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ \"./application/observer/index.js\");\n/* harmony import */ var _hoc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hoc */ \"./application/hoc.js\");\n/* harmony import */ var _classwork_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classwork/observer */ \"./classwork/observer.js\");\n// Точка входа в наше приложение\n\n\n// import CustomEvents from './observer/CustomEvents';\n \nObject(_classwork_observer__WEBPACK_IMPORTED_MODULE_2__[\"default\"])();\n//import demo2 from './observer/demo2';\n//demo2();\n// 0. HOC\n// HOC();\n// 1. Observer ->\n// console.log( Observer );\n// Observer();\n// console.log('INDEX');\n// 2. CustomEvents ->\n// CustomEvents();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer/Observer.js":
/*!******************************************!*\
  !*** ./application/observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\n  // Создаем список подписаных обьектов\n  var observers = [];\n  // Оповещение всех подписчиков о сообщении\n  this.sendMessage = function( msg ){\n      observers.map( ( obs ) => {\n        obs.notify(msg);\n      });\n  };\n  // Добавим наблюдателя\n  this.addObserver = function( observer ){\n    observers.push( observer );\n  };\n}\n// Сам наблюдатель:\nfunction Observer( behavior ){\n  // Делаем функцию, что бы через callback можно\n  // было использовать различные функции внутри\n  this.notify = function( callback ){\n    behavior( callback );\n  };\n}\n\n\n\n\n//# sourceURL=webpack:///./application/observer/Observer.js?");

/***/ }),

/***/ "./application/observer/demo1.js":
/*!***************************************!*\
  !*** ./application/observer/demo1.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\n\nconst Demo1 = () => {\n  console.log( 'DEMO 1 ONLINE');\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\n  let obs1 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.log(msg));\n  let obs2 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.warn(msg));\n  let obs3 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.error(msg));\n  //\n  observable.addObserver( obs1 );\n  observable.addObserver( obs2 );\n  observable.addObserver( obs3 );\n  //\n  console.log( observable );\n  observable.sendMessage('hello');\n\n\n  //  Проверим абстрактно как оно работает:\n  // setTimeout(\n  //   ()=>{\n  //     // оправим сообщение, с текущей датой:\n  //     observable.sendMessage('Now is' + new Date());\n  //   }, 2000\n  // );\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo1);\n\n\n//# sourceURL=webpack:///./application/observer/demo1.js?");

/***/ }),

/***/ "./application/observer/demo2.js":
/*!***************************************!*\
  !*** ./application/observer/demo2.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\n\nconst Demo2 = () => {\n\n  /*\n    Рассмотрим на примере интернет магазина:\n  */\n  console.log( 'DEMO 2');\n  let Products = [\n    {\n      id: 1,\n      name: 'Samsung Galaxy S8 ',\n      price: 21999,\n      imageLink: 'https://i1.rozetka.ua/goods/1894533/samsung_galaxy_s8_64gb_black_images_1894533385.jpg'\n    },\n    {\n      id: 2,\n      name: 'Apple AirPort Capsule',\n      price: 10700,\n      imageLink: 'https://i1.rozetka.ua/goods/3330569/apple_a1470_me177_images_3330569615.jpg'\n    },\n    {\n      id: 3,\n      name: 'Apple iPhone X',\n      price: 35999,\n      imageLink: 'https://i1.rozetka.ua/goods/2433231/apple_iphone_x_64gb_silver_images_2433231297.jpg'\n    },\n    {\n      id: 4,\n      name: 'LG G6 Black ',\n      price: 15999,\n      imageLink: 'https://i1.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329834.jpg'\n    }\n  ];\n\n  // Создадим наблюдателя:\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\n  // Трех обсерверов:\n  let basketObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id){\n    let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\n        // console.log();\n        Cart.push( filtredToBasket[0] );\n        renderBasket();\n  });\n\n  let serverObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\n      let msg = `Товар ${filtredToBasket[0].name} добавлен в корзину`;\n      console.log( msg );\n  });\n\n  let iconObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\n\n      let products__cart = document.getElementById('products__cart');\n          products__cart.innerText = Cart.length;\n  });\n\n  observable.addObserver( basketObs );\n  observable.addObserver( serverObs );\n  observable.addObserver( iconObs );\n\n  // Render Data - - - - - - - - - - - -\n  let Cart = [];\n  let products__row = document.getElementById('products__row');\n\n  function renderBasket(){\n    let cartElem = document.getElementById('cart');\n    let message;\n        if( Cart.length === 0 ){\n          message = 'У вас в корзине пусто';\n        } else {\n          let Sum = Cart.reduce( (prev, current) => {\n            return prev += Number(current.price);\n          }, 0);\n          message = `У вас в корзине ${Cart.length} товаров, на сумму: ${Sum} грн.`;\n        }\n        cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;\n\n        let ol = cartElem.querySelector('ol');\n        Cart.map( item => {\n          let li = document.createElement('li');\n              li.innerText = `${item.name} (${item.price} грн.)`;\n              ol.appendChild(li);\n        });\n  }\n\n  Products.map( item => {\n    let product = document.createElement('div');\n        product.className = \"product\";\n        product.innerHTML =\n        `<div class=\"product__image\">\n            <img src=\"${item.imageLink}\"/>\n          </div>\n          <div class=\"product__name\">${item.name}</div>\n          <div class=\"product__price\">${item.price} грн.</div>\n          <div class=\"product__action\">\n            <button class=\"product__buy\" data-id=${item.id}> Купить </button>\n          </div>`;\n        let buyButton = product.querySelector('.product__buy');\n            buyButton.addEventListener('click', (e) => {\n              let id = e.target.dataset.id;\n              observable.sendMessage(id);\n            });\n        products__row.appendChild(product);\n  });\n\n  renderBasket();\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo2);\n\n\n//# sourceURL=webpack:///./application/observer/demo2.js?");

/***/ }),

/***/ "./application/observer/index.js":
/*!***************************************!*\
  !*** ./application/observer/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _demo1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./demo1 */ \"./application/observer/demo1.js\");\n/* harmony import */ var _demo2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./demo2 */ \"./application/observer/demo2.js\");\n\n\n\nconst ObserverDemo = () => {\n  // Abstract Demo 2\n  // console.log('demo 1');\n  // Demo1();\n\n  // Functional Demo:\n  // Demo2();\n\n}; //observer Demo\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObserverDemo);\n\n\n//# sourceURL=webpack:///./application/observer/index.js?");

/***/ }),

/***/ "./classwork/observer.js":
/*!*******************************!*\
  !*** ./classwork/observer.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/observer/Observer */ \"./application/observer/Observer.js\");\n/*\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\n\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\n    песню в исполнение иди добавить в плейлист.\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\n\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\n    пишет суммарное время проигрывания всех песен в плейлисте.\n\n    3. Отображает песню которая проигрывается.\n\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\n    с возможностью его остановки.\n*/\nconst MusicList = [\n  {\n    title: 'Rammstein',\n    songs: [\n      {\n        id: 1,\n        name: 'Du Hast',\n        time: [3, 12]\n      },\n      {\n        id: 2,\n        name: 'Ich Will',\n        time: [5, 1]\n      },\n      {\n        id: 3,\n        name: 'Mutter',\n        time: [4, 15]\n      },\n      {\n        id: 4,\n        name: 'Ich tu dir weh',\n        time: [5, 13]\n      },\n      {\n        id: 5,\n        name: 'Rammstein',\n        time: [3, 64]\n      }\n    ]\n  },\n  {\n    title: 'System of a Down',\n    songs: [\n      {\n        id: 6,\n        name: 'Toxicity',\n        time: [4, 22]\n      },\n      {\n        id: 7,\n        name: 'Sugar',\n        time: [2, 44]\n      },\n      {\n        id: 8,\n        name: 'Lonely Day',\n        time: [3, 19]\n      },\n      {\n        id: 9,\n        name: 'Lost in Hollywood',\n        time: [5, 9]\n      },\n      {\n        id: 10,\n        name: 'Chop Suey!',\n        time: [2, 57]\n      }\n    ]\n  },\n  {\n    title: 'Green Day',\n    songs: [\n      {\n        id: 11,\n        name: '21 Guns',\n        time: [4, 16]\n      },\n      {\n        id: 12,\n        name: 'Boulevard of broken dreams!',\n        time: [6, 37]\n      },\n      {\n        id: 13,\n        name: 'Basket Case!',\n        time: [3, 21]\n      },\n      {\n        id: 14,\n        name: 'Know Your Enemy',\n        time: [4, 11]\n      }\n    ]\n  },\n  {\n    title: 'Linkin Park',\n    songs: [\n      {\n        id: 15,\n        name: 'Numb',\n        time: [3, 11]\n      },\n      {\n        id: 16,\n        name: 'New Divide',\n        time: [4, 41]\n      },\n      {\n        id: 17,\n        name: 'Breaking the Habit',\n        time: [4, 1]\n      },\n      {\n        id: 18,\n        name: 'Faint',\n        time: [3, 29]\n      }\n    ]\n  }\n]\n\nconst MusicBox = () => {\n  const MusicBox = document.getElementById('MusicBox');\n    const PlaylistDiv = document.getElementById('Playlist');\n    const DurationDiv = document.getElementById('Duration');\n    let playlist = [];\n    let btndisabled =[];\n    let playingnow = [];\n\n\n  const renderList =()=>{\n      MusicBox.innerHTML = '';\n      MusicList.map( Artist => {\n          MusicBox.innerHTML += `<div><h4>${Artist.title}</h4><ul>`;\n          Artist.songs.map( song => {\n              MusicBox.innerHTML += `<li>${song.name} <button class=\"add\" data-id=\"${song.id}\">Add</button></li>`;\n          MusicBox.innerHTML += '</ul></div>';\n              let addBtns = document.querySelectorAll('.add');\n\n          btndisabled.map(button => {\n              addBtns.forEach(btn => { if(Number(button.dataset.id) === Number(btn.dataset.id)){\n                  btn.setAttribute(\"disabled\",\"disabled\");\n              }});\n          })\n      });\n\n      });\n      let addbuttons = MusicBox.querySelectorAll('.add');\n      addbuttons.forEach((btn)=>{\n        btn.addEventListener('click',(e)=>{\n            let id =[];\n            id[1] = Number(e.target.dataset.id);\n            id[0] = 1;\n            observable.sendMessage(id);\n        })\n      })\n\n  }\n  renderList();\n\n\n    // Создадим наблюдателя:\n    let observable = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\n    // Трех обсерверов:\n    let playlistObs = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id){\n//        console.log(id);\n        let filtredMusic =[] ;\n        MusicList.map(artist =>{artist.songs.map(song => {if(Number(song.id) === Number(id[1]) && Number(id[0]) === 1)\n        {filtredMusic.push(song);\n            playlist.push(filtredMusic[0]);\n        //    console.log(playlist);\n            renderPlaylist();\n        }else if(Number(song.id) === Number(id[1]) && Number(id[0]) === 3){\n            playlist.splice(playlist.length - 1,1);\n            renderPlaylist();\n        }\n\n        })});\n\n    });\n\n    let musicboxObs = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\n        let addBtns = document.querySelectorAll('.add');\n        console.log(id);\n        addBtns.forEach(btn =>{ if(Number(btn.dataset.id) === Number(id[1]) &&  Number(id[0])!== 3){\n           // console.log(btn);\n            btn.setAttribute(\"disabled\",\"disabled\");\n            btndisabled.push(btn);\n            renderList();\n            console.log(\"yes\",btndisabled);\n        }else if(Number(btn.dataset.id) === Number(id[1]) &&  Number(id[0]) === 3 ){\n\n            btndisabled.forEach((button)=>\n            {\n                if(Number(btn.dataset.id) === Number(id[1]))\n                {\n                    console.log(\"No\",btndisabled);\n                    btn.removeAttribute(\"disabled\");\n                    console.log(button.index);\n                    btndisabled.reverse();\n                    btndisabled.splice(button.index,1);\n                }\n\n\n            })\n            renderList();\n        }\n        });\n\n\n    });\n\n    let durationObs = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]((id) => {\n       let filtred = [];\n        playlist.map(song =>{if(Number(song.id) === Number(id[1]) && Number(id[0]) === 2)\n        {\n            playingnow.splice(0,playingnow.length);\n            filtred.push(song);\n            playingnow.push(filtred[0]);\n        }else if(Number(song.id) === Number(id[1]) && Number(id[0]) === 3){\n\n        }});\n\n        renderDuration();\n    });\n\n    observable.addObserver( playlistObs );\n    observable.addObserver( musicboxObs );\n    observable.addObserver( durationObs );\n\n\n\n    const renderPlaylist =()=>{\n        console.log(playlist);\n        PlaylistDiv.innerHTML ='';\n        PlaylistDiv.innerHTML +='<h2>Playlist</h2>';\n        PlaylistDiv.innerHTML += '<ul>';\n        playlist.map( song =>\n        {\n            PlaylistDiv.innerHTML += `<li>${song.name} ${song.time}\n<button class=\"play\" data-id=\"${song.id}\">Play</button>\n<button data-id=\"${song.id}\" class=\"remove\">Remove</button>\n</li>`;\n        });\n            PlaylistDiv.innerHTML += '</ul>';\n\n        let playbtns = PlaylistDiv.querySelectorAll('.play');\n        playbtns.forEach((btn)=>{\n            btn.addEventListener('click',(e)=>{\n                let id =[];\n                 id[1] = Number(e.target.dataset.id);\n                 id[0] = 2;\n                observable.sendMessage(id);\n            })\n        })\n        let removebtns = PlaylistDiv.querySelectorAll('.remove');\n        removebtns.forEach((btn)=>{\n            btn.addEventListener('click',(e)=>{\n                let id =[];\n                id[1] = Number(e.target.dataset.id);\n                id[0] = 3;\n                observable.sendMessage(id);\n            })\n        })\n\n    };\n    const renderDuration =()=>{\n\n        DurationDiv.innerHTML ='';\n        DurationDiv.innerHTML +='<h2>Is Playing Now</h2>';\n        DurationDiv.innerHTML += '<ul>';\n        playingnow.map( song =>\n        {\n                    DurationDiv.innerHTML += `<li>${song.name}  duration: ${song.time}</li>`;\n\n        });\n        DurationDiv.innerHTML += '</ul>';\n\n\n\n    }\n\n\n\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\n\n\n//# sourceURL=webpack:///./classwork/observer.js?");

/***/ })

/******/ });