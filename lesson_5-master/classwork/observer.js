/*
  Задание: Модуль создания плейлиста, используя паттерн Обсервер.

  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:
    1. Список исполнителей и песен (Находится слева) - отуда можно включить
    песню в исполнение иди добавить в плейлист.
    Если песня уже есть в плейлисте, дважды добавить её нельзя.

    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,
    или запустить в исполнение. Внизу списка должен выводиться блок, в котором
    пишет суммарное время проигрывания всех песен в плейлисте.

    3. Отображает песню которая проигрывается.

    4. + Бонус: Сделать прогресс пар того как проигрывается песня
    с возможностью его остановки.
*/
const MusicList = [
  {
    title: 'Rammstein',
    songs: [
      {
        id: 1,
        name: 'Du Hast',
        time: [3, 12]
      },
      {
        id: 2,
        name: 'Ich Will',
        time: [5, 1]
      },
      {
        id: 3,
        name: 'Mutter',
        time: [4, 15]
      },
      {
        id: 4,
        name: 'Ich tu dir weh',
        time: [5, 13]
      },
      {
        id: 5,
        name: 'Rammstein',
        time: [3, 64]
      }
    ]
  },
  {
    title: 'System of a Down',
    songs: [
      {
        id: 6,
        name: 'Toxicity',
        time: [4, 22]
      },
      {
        id: 7,
        name: 'Sugar',
        time: [2, 44]
      },
      {
        id: 8,
        name: 'Lonely Day',
        time: [3, 19]
      },
      {
        id: 9,
        name: 'Lost in Hollywood',
        time: [5, 9]
      },
      {
        id: 10,
        name: 'Chop Suey!',
        time: [2, 57]
      }
    ]
  },
  {
    title: 'Green Day',
    songs: [
      {
        id: 11,
        name: '21 Guns',
        time: [4, 16]
      },
      {
        id: 12,
        name: 'Boulevard of broken dreams!',
        time: [6, 37]
      },
      {
        id: 13,
        name: 'Basket Case!',
        time: [3, 21]
      },
      {
        id: 14,
        name: 'Know Your Enemy',
        time: [4, 11]
      }
    ]
  },
  {
    title: 'Linkin Park',
    songs: [
      {
        id: 15,
        name: 'Numb',
        time: [3, 11]
      },
      {
        id: 16,
        name: 'New Divide',
        time: [4, 41]
      },
      {
        id: 17,
        name: 'Breaking the Habit',
        time: [4, 1]
      },
      {
        id: 18,
        name: 'Faint',
        time: [3, 29]
      }
    ]
  }
]
import { Observable, Observer } from '../application/observer/Observer';
const MusicBox = () => {
  const MusicBox = document.getElementById('MusicBox');
    const PlaylistDiv = document.getElementById('Playlist');
    const DurationDiv = document.getElementById('Duration');
    let playlist = [];
    let btndisabled =[];
    let playingnow = [];


  const renderList =()=>{
      MusicBox.innerHTML = '';
      MusicList.map( Artist => {
          MusicBox.innerHTML += `<div><h4>${Artist.title}</h4><ul>`;
          Artist.songs.map( song => {
              MusicBox.innerHTML += `<li>${song.name} <button class="add" data-id="${song.id}">Add</button></li>`;
          MusicBox.innerHTML += '</ul></div>';
              let addBtns = document.querySelectorAll('.add');

          btndisabled.map(button => {
              addBtns.forEach(btn => { if(Number(button.dataset.id) === Number(btn.dataset.id)){
                  btn.setAttribute("disabled","disabled");
              }});
          })
      });

      });
      let addbuttons = MusicBox.querySelectorAll('.add');
      addbuttons.forEach((btn)=>{
        btn.addEventListener('click',(e)=>{
            let id =[];
            id[1] = Number(e.target.dataset.id);
            id[0] = 1;
            observable.sendMessage(id);
        })
      })

  }
  renderList();


    // Создадим наблюдателя:
    let observable = new Observable();
    // Трех обсерверов:
    let playlistObs = new Observer( function(id){
//        console.log(id);
        let filtredMusic =[] ;
        MusicList.map(artist =>{artist.songs.map(song => {if(Number(song.id) === Number(id[1]) && Number(id[0]) === 1)
        {filtredMusic.push(song);
            playlist.push(filtredMusic[0]);
        //    console.log(playlist);
            renderPlaylist();
        }else if(Number(song.id) === Number(id[1]) && Number(id[0]) === 3){
            playlist.splice(playlist.length - 1,1);
            renderPlaylist();
        }

        })});

    });

    let musicboxObs = new Observer( (id) => {
        let addBtns = document.querySelectorAll('.add');
        console.log(id);
        addBtns.forEach(btn =>{ if(Number(btn.dataset.id) === Number(id[1]) &&  Number(id[0])!== 3){
           // console.log(btn);
            btn.setAttribute("disabled","disabled");
            btndisabled.push(btn);
            renderList();
            console.log("yes",btndisabled);
        }else if(Number(btn.dataset.id) === Number(id[1]) &&  Number(id[0]) === 3 ){

            btndisabled.forEach((button)=>
            {
                if(Number(btn.dataset.id) === Number(id[1]))
                {
                    console.log("No",btndisabled);
                    btn.removeAttribute("disabled");
                    console.log(button.index);
                    btndisabled.reverse();
                    btndisabled.splice(button.index,1);
                }


            })
            renderList();
        }
        });


    });

    let durationObs = new Observer((id) => {
       let filtred = [];
        playlist.map(song =>{if(Number(song.id) === Number(id[1]) && Number(id[0]) === 2)
        {
            playingnow.splice(0,playingnow.length);
            filtred.push(song);
            playingnow.push(filtred[0]);
        }else if(Number(song.id) === Number(id[1]) && Number(id[0]) === 3){

        }});

        renderDuration();
    });

    observable.addObserver( playlistObs );
    observable.addObserver( musicboxObs );
    observable.addObserver( durationObs );



    const renderPlaylist =()=>{
        console.log(playlist);
        PlaylistDiv.innerHTML ='';
        PlaylistDiv.innerHTML +='<h2>Playlist</h2>';
        PlaylistDiv.innerHTML += '<ul>';
        playlist.map( song =>
        {
            PlaylistDiv.innerHTML += `<li>${song.name} ${song.time}
<button class="play" data-id="${song.id}">Play</button>
<button data-id="${song.id}" class="remove">Remove</button>
</li>`;
        });
            PlaylistDiv.innerHTML += '</ul>';

        let playbtns = PlaylistDiv.querySelectorAll('.play');
        playbtns.forEach((btn)=>{
            btn.addEventListener('click',(e)=>{
                let id =[];
                 id[1] = Number(e.target.dataset.id);
                 id[0] = 2;
                observable.sendMessage(id);
            })
        })
        let removebtns = PlaylistDiv.querySelectorAll('.remove');
        removebtns.forEach((btn)=>{
            btn.addEventListener('click',(e)=>{
                let id =[];
                id[1] = Number(e.target.dataset.id);
                id[0] = 3;
                observable.sendMessage(id);
            })
        })

    };
    const renderDuration =()=>{

        DurationDiv.innerHTML ='';
        DurationDiv.innerHTML +='<h2>Is Playing Now</h2>';
        DurationDiv.innerHTML += '<ul>';
        playingnow.map( song =>
        {
                    DurationDiv.innerHTML += `<li>${song.name}  duration: ${song.time}</li>`;

        });
        DurationDiv.innerHTML += '</ul>';



    }




}

export default MusicBox;
