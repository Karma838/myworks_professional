/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

const Compose = () => {

    const MakeBackendMagic = (state) => ({
        backend: () => console.log(state.name + ' is making backend')
    });

    const MakeFrontendMagic = state => ({
        frontend: () => console.log(state.name + ' is making frontend')
    });

    const DistributeTasks = (state) => ({
        distribute: () => console.log(state.name + ' is distributing tasks')
    });

    const DrinkSomeTea = (state) => ({
        drink: () => console.log(state.name + ' is having a break')
    });

    const WatchYoutube = (state) => ({
        watchYoutube: () => console.log(state.name + ' is youtubing :)')

    });

    const Procrastinate = (state) => ({
        procrastinate: () => {
            console.log(state.name + 'procrastinates every day');
        }
    });


    const BackendDeveloper = (name, age, gender) => {
        let object = {
            name,
            age,
            gender
        };

        return Object.assign(
            {},
            object,
            MakeBackendMagic(object),
            DrinkSomeTea(object),
            Procrastinate(object)
        );
    };
    const FrontDeveloper = (name, age, gender) => {
        let object = {
            name,
            age,
            gender
        };

        return Object.assign(
            {},
            object,
            MakeFrontendMagic(object),
            DrinkSomeTea(object),
            WatchYoutube(object)
        );
    };

    let backdev = BackendDeveloper("Kolya", 22, "Male");
    backdev.drink();
    let frontdev = FrontDeveloper("Nazar", 19, "Male");
    frontdev.frontend();
}
Compose();
export default Compose;