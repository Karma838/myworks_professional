/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/
const Task2 =()=> {
    let availableEmps = [];
    let hiredEmps = [];

    const MakeItLooksBeautiful = (state) => ({
        beautiful: () => console.log(state.name + ' is making this shit beautiful')
    });

    const MakeBackendMagic = (state) => ({
        backend: () => console.log(state.name + ' is making backend')
    });

    const MakeFrontendMagic = state => ({
        frontend: () => console.log(state.name + ' is making frontend')
    });

    const DistributeTasks = (state) => ({
        distribute: () => console.log(state.name + ' is distributing tasks')
    });

    const DrinkSomeTea = (state) => ({
        drink: () => console.log(state.name + ' is having a break')
    });

    const WatchYoutube = (state) => ({
        watchYoutube: () => console.log(state.name + ' is youtubing :)')

    });

    const Procrastinate = (state) => ({
        procrastinate: () => {
            console.log(state.name + 'procrastinates every day');
        }
    });
    class PeopleFactory {
        makeEmployee(employ) {
            if (employ.type === 'backend') {
                employ = BackendDeveloper(employ.name, employ.age, employ.gender, employ.type, employ.picture);
            } else if (employ.type === 'frontend') {
                employ = FrontDeveloper(employ.name, employ.age, employ.gender, employ.type, employ.picture);
            } else if (employ.type === 'project') {
                employ = ProjectManager(employ.name, employ.age, employ.gender, employ.type, employ.picture);
            } else if (employ.type === 'design') {
                employ = Designer(employ.name, employ.age, employ.gender, employ.type, employ.picture);
            } else {
                return false;
            }
            return employ;
        }
    }

    const BackendDeveloper = (name, age, gender, type, picture) => {
        //  let type = "back";
        let object = {
            name,
            age,
            gender,
            type,
            picture
        };

        return Object.assign(
            {},
            object,
            MakeBackendMagic(object),
            DrinkSomeTea(object),
            Procrastinate(object),
            //  render()
        );
    };
    const Designer = (name, age, gender, type, picture) => {
        let object = {
            name,
            age,
            gender,
            type,
            picture
        };

        return Object.assign(
            {},
            object,
            MakeItLooksBeautiful(object),
            WatchYoutube(object),
            Procrastinate(object),
            //render()
        );
    };

    const ProjectManager = (name, age, gender, type, picture) => {
        // let type = "Front";
        let object = {
            name,
            age,
            gender,
            type,
            picture
        };

        return Object.assign(
            {},
            object,
            DistributeTasks(object),
            Procrastinate(object),
            DrinkSomeTea(object),
            //  render()
        );
    };

    const FrontDeveloper = (name, age, gender, type, picture) => {
        // let type = "Front";
        let object = {
            name,
            age,
            gender,
            type,
            picture
        };

        return Object.assign(
            {},
            object,
            MakeFrontendMagic(object),
            DrinkSomeTea(object),
            WatchYoutube(object),
            //  render()
        );
    };

    const mySuperForge = new PeopleFactory();

    let url = "http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2";
    let data = fetch(url)
        .then(data => {
            let employees = data.json();
            //  console.log("Users: ",employees);
            return employees;
        }).then(employees => {
            let table = document.createElement("table");
            table.style.padding = "20px";
            let innerTable = `<tr>
    <th>Avatar</th>
    <th>Full Name</th>
    <th>Age</th>
    <th>Occupation</th>
  </tr>`;
            const root = document.getElementById('root');
            table.innerHTML = innerTable;
            employees.forEach(item => {
                let index = employees.indexOf(item);
                let emp = mySuperForge.makeEmployee(item);
                availableEmps.push(emp);

                const render = () => {


                    table.innerHTML += `
          <tr><td><img src="${emp.picture}"/></td><td>${emp.name} </td>
    <td>(${emp.age})</td>
    <td>${emp.type}</td>
    <td><button class="hire" data-id="${index}"> Нанять </button></td></tr>       
`;

                };

                render();


            });
            root.appendChild(table);

            let hirebtns = root.querySelectorAll('.hire');
            const hired = document.getElementById('team');
            hirebtns.forEach(btn=>{

                btn.addEventListener('click',()=>{

                    availableEmps.map(emp => {
                        let index = availableEmps.indexOf(emp);
                        if(index == btn.dataset.id) {
                            console.log("pressed", btn.dataset.id);
                            hiredEmps.push(emp);
                           delete availableEmps[index];
                         //   availableEmps.delete(index);
                            btn.setAttribute("disabled","disabled");
                            const render = () => {
                                hired.innerHTML += `
          <tr><td><img src="${emp.picture}"/></td><td>${emp.name} </td>
    <td>(${emp.age})</td>
    <td>${emp.type}</td>
    </tr><br>
`;};
                            render();
                        }
                    })

                 /*   availableEmps.forEach(emp=>{
                        let index = availableEmps.indexOf(emp);

                        const render = () => {


                            table.innerHTML += `
          <tr><td><img src="${emp.picture}"/></td><td>${emp.name} </td>
    <td>(${emp.age})</td>
    <td>${emp.type}</td>
    <td><button class="hire" data-id="${index}"> Нанять </button></td></tr>       
`;

                        };

                        render();

                    })*/



                })
            })
        });


}
export default Task2;