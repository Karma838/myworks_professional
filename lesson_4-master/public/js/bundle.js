/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_task2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/task2 */ \"./classwork/task2.js\");\n//import Task1 from \"../classwork/task1\"\n\n\n\n//Task1();\nObject(_classwork_task2__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/task2.js":
/*!****************************!*\
  !*** ./classwork/task2.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\n  расспределять и создавать сотрудников компании нужного типа.\n\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\n\n  HeadHunt => {\n    hire( obj ){\n      ...\n    }\n  }\n\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\n\n*/\nconst Task2 =()=> {\n    let availableEmps = [];\n    let hiredEmps = [];\n\n    const MakeItLooksBeautiful = (state) => ({\n        beautiful: () => console.log(state.name + ' is making this shit beautiful')\n    });\n\n    const MakeBackendMagic = (state) => ({\n        backend: () => console.log(state.name + ' is making backend')\n    });\n\n    const MakeFrontendMagic = state => ({\n        frontend: () => console.log(state.name + ' is making frontend')\n    });\n\n    const DistributeTasks = (state) => ({\n        distribute: () => console.log(state.name + ' is distributing tasks')\n    });\n\n    const DrinkSomeTea = (state) => ({\n        drink: () => console.log(state.name + ' is having a break')\n    });\n\n    const WatchYoutube = (state) => ({\n        watchYoutube: () => console.log(state.name + ' is youtubing :)')\n\n    });\n\n    const Procrastinate = (state) => ({\n        procrastinate: () => {\n            console.log(state.name + 'procrastinates every day');\n        }\n    });\n    class PeopleFactory {\n        makeEmployee(employ) {\n            if (employ.type === 'backend') {\n                employ = BackendDeveloper(employ.name, employ.age, employ.gender, employ.type, employ.picture);\n            } else if (employ.type === 'frontend') {\n                employ = FrontDeveloper(employ.name, employ.age, employ.gender, employ.type, employ.picture);\n            } else if (employ.type === 'project') {\n                employ = ProjectManager(employ.name, employ.age, employ.gender, employ.type, employ.picture);\n            } else if (employ.type === 'design') {\n                employ = Designer(employ.name, employ.age, employ.gender, employ.type, employ.picture);\n            } else {\n                return false;\n            }\n            return employ;\n        }\n    }\n\n    const BackendDeveloper = (name, age, gender, type, picture) => {\n        //  let type = \"back\";\n        let object = {\n            name,\n            age,\n            gender,\n            type,\n            picture\n        };\n\n        return Object.assign(\n            {},\n            object,\n            MakeBackendMagic(object),\n            DrinkSomeTea(object),\n            Procrastinate(object),\n            //  render()\n        );\n    };\n    const Designer = (name, age, gender, type, picture) => {\n        let object = {\n            name,\n            age,\n            gender,\n            type,\n            picture\n        };\n\n        return Object.assign(\n            {},\n            object,\n            MakeItLooksBeautiful(object),\n            WatchYoutube(object),\n            Procrastinate(object),\n            //render()\n        );\n    };\n\n    const ProjectManager = (name, age, gender, type, picture) => {\n        // let type = \"Front\";\n        let object = {\n            name,\n            age,\n            gender,\n            type,\n            picture\n        };\n\n        return Object.assign(\n            {},\n            object,\n            DistributeTasks(object),\n            Procrastinate(object),\n            DrinkSomeTea(object),\n            //  render()\n        );\n    };\n\n    const FrontDeveloper = (name, age, gender, type, picture) => {\n        // let type = \"Front\";\n        let object = {\n            name,\n            age,\n            gender,\n            type,\n            picture\n        };\n\n        return Object.assign(\n            {},\n            object,\n            MakeFrontendMagic(object),\n            DrinkSomeTea(object),\n            WatchYoutube(object),\n            //  render()\n        );\n    };\n\n    const mySuperForge = new PeopleFactory();\n\n    let url = \"http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\";\n    let data = fetch(url)\n        .then(data => {\n            let employees = data.json();\n            //  console.log(\"Users: \",employees);\n            return employees;\n        }).then(employees => {\n            let table = document.createElement(\"table\");\n            table.style.padding = \"20px\";\n            let innerTable = `<tr>\n    <th>Avatar</th>\n    <th>Full Name</th>\n    <th>Age</th>\n    <th>Occupation</th>\n  </tr>`;\n            const root = document.getElementById('root');\n            table.innerHTML = innerTable;\n            employees.forEach(item => {\n                let index = employees.indexOf(item);\n                let emp = mySuperForge.makeEmployee(item);\n                availableEmps.push(emp);\n\n                const render = () => {\n\n\n                    table.innerHTML += `\n          <tr><td><img src=\"${emp.picture}\"/></td><td>${emp.name} </td>\n    <td>(${emp.age})</td>\n    <td>${emp.type}</td>\n    <td><button class=\"hire\" data-id=\"${index}\"> Нанять </button></td></tr>       \n`;\n\n                };\n\n                render();\n\n\n            });\n            root.appendChild(table);\n\n            let hirebtns = root.querySelectorAll('.hire');\n            const hired = document.getElementById('team');\n            hirebtns.forEach(btn=>{\n\n                btn.addEventListener('click',()=>{\n\n                    availableEmps.map(emp => {\n                        let index = availableEmps.indexOf(emp);\n                        if(index == btn.dataset.id) {\n                            console.log(\"pressed\", btn.dataset.id);\n                            hiredEmps.push(emp);\n                           delete availableEmps[index];\n                         //   availableEmps.delete(index);\n                            btn.setAttribute(\"disabled\",\"disabled\");\n                            const render = () => {\n                                hired.innerHTML += `\n          <tr><td><img src=\"${emp.picture}\"/></td><td>${emp.name} </td>\n    <td>(${emp.age})</td>\n    <td>${emp.type}</td>\n    </tr><br>\n`;};\n                            render();\n                        }\n                    })\n\n                 /*   availableEmps.forEach(emp=>{\n                        let index = availableEmps.indexOf(emp);\n\n                        const render = () => {\n\n\n                            table.innerHTML += `\n          <tr><td><img src=\"${emp.picture}\"/></td><td>${emp.name} </td>\n    <td>(${emp.age})</td>\n    <td>${emp.type}</td>\n    <td><button class=\"hire\" data-id=\"${index}\"> Нанять </button></td></tr>       \n`;\n\n                        };\n\n                        render();\n\n                    })*/\n\n\n\n                })\n            })\n        });\n\n\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (Task2);\n\n//# sourceURL=webpack:///./classwork/task2.js?");

/***/ })

/******/ });