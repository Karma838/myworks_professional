/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он пренадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/

const Mediator = () => {


  class Professor{
    constructor(name) {
        this.name = name;
        this.answer = '...';
    }
    answerTheQuestion( student, question){
      if( student.type !== 'monitor'){
        console.error('It\' not your business');
      } else {
        console.log('Yes, my dear?', question);
        this.answer = "I wanna drink some coffee";
          this.answer = '...';
      }
    }
  }

  class Student {
      constructor( name ){
          this.name = name;
          this.monitor = null;
          this.answer = '';
      }
      getAnswer(){
          console.log(`${this.monitor.name} to ${this.name}: ${this.answer}`);
      }
      sayToMonitor( message, to ){
          if( this.monitor !== null){
         let  from = this.monitor;
              this.monitor.askProfessor( message,from, to );
              console.log("to",to,"from",from,"message",message);
              this.answer = to.answer;
          } else {
              console.warn(`${this.name} - you can't send message, until you dont ask monitor`);
          }
      }
  }

  // Monitor == Староста
  class Monitor
  {
      constructor( name ){
          this.name = name;
          this.students = {};
          this.type = "monitor";
      }
    addToGroup(student){
        this.students[student.name] = student;
        student.monitor = this;

        console.log(`New student' ${student.name}' registered by monitor ${name}`);
        console.log('List or registered:', this.students );
    }
    askProfessor(message, from, to){
      if( to !== undefined ){
        to.answerTheQuestion( this, message );
      } else {
        for( let key in this.students ){
          if( this.students[key] === from ){
            this.students[key].sayToMonitor(message, from);
          }
        }
      }
    }
    }

    let Kolya = new Student('Kolya');
    let Vasya = new Student('Vasya');
    let Petya = new Student('Petya');
    const Captain = new Monitor('Captain');
    const Proff = new Professor("Ivan Ivanovich");
    console.log('- - - - - - - - -');
    Captain.addToGroup(Kolya);
    Captain.addToGroup(Vasya);
    Captain.addToGroup(Petya);
    console.log('- - - - - - - - -');
    Kolya.sayToMonitor("What is my mark?",Proff);
    Kolya.getAnswer("my quest");


}

export default Mediator;
