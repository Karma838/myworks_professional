/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _mediator = __webpack_require__(/*! ../classworks/mediator */ \"./classworks/mediator.js\");\n\nvar _mediator2 = _interopRequireDefault(_mediator);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n(0, _mediator2.default)();\n//Command();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/mediator.js":
/*!********************************!*\
  !*** ./classworks/mediator.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/*\n  Написать медиатор для группы студентов.\n\n  Профессор отвечает только на вопросы старосты.\n\n  У Студента есть имя и группа которой он пренадлежит.\n  Он может запросить старосту задать вопрос или получить ответ.\n\n  Староста может добавлять студентов в группу и передавать вопрос профессору.\n*/\n\nvar Mediator = function Mediator() {\n  var Professor = function () {\n    function Professor(name) {\n      _classCallCheck(this, Professor);\n\n      this.name = name;\n      this.answer = '...';\n    }\n\n    _createClass(Professor, [{\n      key: 'answerTheQuestion',\n      value: function answerTheQuestion(student, question) {\n        if (student.type !== 'monitor') {\n          console.error('It\\' not your business');\n        } else {\n          console.log('Yes, my dear?', question);\n          this.answer = \"I wanna drink some coffee\";\n          this.answer = '...';\n        }\n      }\n    }]);\n\n    return Professor;\n  }();\n\n  var Student = function () {\n    function Student(name) {\n      _classCallCheck(this, Student);\n\n      this.name = name;\n      this.monitor = null;\n      this.answer = '';\n    }\n\n    _createClass(Student, [{\n      key: 'getAnswer',\n      value: function getAnswer() {\n        console.log(this.monitor.name + ' to ' + this.name + ': ' + this.answer);\n      }\n    }, {\n      key: 'sayToMonitor',\n      value: function sayToMonitor(message, to) {\n        if (this.monitor !== null) {\n          var from = this.monitor;\n          this.monitor.askProfessor(message, from, to);\n          console.log(\"to\", to, \"from\", from, \"message\", message);\n          this.answer = to.answer;\n        } else {\n          console.warn(this.name + ' - you can\\'t send message, until you dont ask monitor');\n        }\n      }\n    }]);\n\n    return Student;\n  }();\n\n  // Monitor == Староста\n\n\n  var Monitor = function () {\n    function Monitor(name) {\n      _classCallCheck(this, Monitor);\n\n      this.name = name;\n      this.students = {};\n      this.type = \"monitor\";\n    }\n\n    _createClass(Monitor, [{\n      key: 'addToGroup',\n      value: function addToGroup(student) {\n        this.students[student.name] = student;\n        student.monitor = this;\n\n        console.log('New student\\' ' + student.name + '\\' registered by monitor ' + name);\n        console.log('List or registered:', this.students);\n      }\n    }, {\n      key: 'askProfessor',\n      value: function askProfessor(message, from, to) {\n        if (to !== undefined) {\n          to.answerTheQuestion(this, message);\n        } else {\n          for (var key in this.students) {\n            if (this.students[key] === from) {\n              this.students[key].sayToMonitor(message, from);\n            }\n          }\n        }\n      }\n    }]);\n\n    return Monitor;\n  }();\n\n  var Kolya = new Student('Kolya');\n  var Vasya = new Student('Vasya');\n  var Petya = new Student('Petya');\n  var Captain = new Monitor('Captain');\n  var Proff = new Professor(\"Ivan Ivanovich\");\n  console.log('- - - - - - - - -');\n  Captain.addToGroup(Kolya);\n  Captain.addToGroup(Vasya);\n  Captain.addToGroup(Petya);\n  console.log('- - - - - - - - -');\n  Kolya.sayToMonitor(\"What is my mark?\", Proff);\n  Kolya.getAnswer(\"my quest\");\n};\n\nexports.default = Mediator;\n\n//# sourceURL=webpack:///./classworks/mediator.js?");

/***/ })

/******/ });